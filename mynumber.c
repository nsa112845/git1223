#include <stdio.h>

int isprime(int n){
    for(int i=2;i<n;i++){
        if(n%i == 0){
            return 0;
        }
    }
    return 1;
}

int main(void)
{

    for(int i=1;i<100;i++){
        if(i%2 == 1){
            printf("%d ",i);
        }
    }
    printf("\n");

    for(int i=1;i<=100;i++){
        if(i%2 == 0){
            printf("%d ",i);
        }
    }
    printf("\n");

    for(int i=2;i<100;i++){
        if(isprime(i)){
            printf("%d ",i);
        }
    }
    printf("\n");

    return 0;
}

